from django.shortcuts import render
from django.core.urlresolvers import reverse

# Create your views here.
from products.models import Product
from categories.models import Category

def home(request, cat=None):
    title = "The Imaginary Shop"
    head = "Home"
    absolute_path = reverse("home-view")
    bread = ["home"]
    qs_top = Product.objects.all().order_by('-sold')[:4]
    info = "The only Imaginary shop that does not exist"
    try:
        cat_all = Category.objects.all().order_by('name')
        cat_top = Category.objects.filter(parent__isnull=True).order_by('name')
    except:
        cat_all = Category.objects.all()
        cat_top = Category.objects.all()

    context = {
        "page_title": title,
        "head_title": head,
        "path": absolute_path,
        "breadcrumb": bread,
        "page_info": info,
        "prod_top": qs_top,
        "all_cat": cat_all,
        "top_cat": cat_top
    }
    return render(request, "home.html", context)