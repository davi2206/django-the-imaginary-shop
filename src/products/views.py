from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import get_object_or_404
from itertools import chain

from .models import Product
from categories.models import Category

'''
# Create your views here.
def top_products(request):
    qs_all = Product.objects.all().order_by('sold')
    qs_top = Product.objects.all().order_by('sold')[:4]

    context = {
    }
    return render(request, "home.html", context)
'''

# Decides weather to show products in a category or details for product
def product_view(request, hierarchy=None):
    categories = hierarchy.split('/')
    category_queryset = list(Category.objects.all())
    all_slugs = [x.name for x in category_queryset]

    category_obj = get_category_obj(categories, categories[-1])

    try:
        if (category_obj) and category_obj.name in all_slugs:
            return product_list_view(request, categories)
        else:
            prod_id = categories[-1]
            prod_cat = get_category_obj(None, categories[-2])
            # Render prod details
            return product_detail_view(request, prod_id, prod_cat)
    except:
        return search_view(request, categories[-1])

# List of products for category / sub category
def product_list_view(request, categories=None):
    request_params = request.GET
    sort_by = request_params.get("sort")

    prim_cat = get_category_obj(categories, categories[-1])
    top_cat = get_category_obj(None, categories[0])
    request.session['top_cat'] = top_cat.id

    try:
        prim_safe = prim_cat.safe_name
    except:
        prim_safe = ""

    title           = prim_cat.name
    head            = prim_cat.name

    sub_cats        = Category.objects.filter(parent__name=prim_cat.name)

    prods = list(get_products(prim_cat))

    carusel_images = []

    for prod in prods[:5]:
        if prod.image: carusel_images.append(prod.image)

    context = {
        "page_title": title,
        "head_title": head,
        "prods_cat": prods,
        "side_menu": sub_cats,
        "prod_images": carusel_images,
        "parent_cat": prim_safe,
        "top_cat": top_cat,
    }
    return render(request, "products/product_list.html", context)

# Detailed representation of Product
#def product_detail_parent_view(request, parent=None, id=None):
#    return product_detail_view(request, parent, None, id)

#Shows detailed view for product
def product_detail_view(request, id=None, cat=None):
    product = get_object_or_404(Product, id=id)
    prods = get_products(cat)

    all_related_prods = Product.objects.filter(prim_category=product.prim_category).exclude(id=product.id)[:5]
    x = all_related_prods.count()

    if x < 5:
        x = 5 - all_related_prods.count()
        all_prods = Product.objects.exclude(id=product.id)[0:x]
        all_related_prods = list(chain(all_related_prods, all_prods))
        related_prods = []
        for p in all_related_prods:
            if p not in related_prods: related_prods.append(p)


    if not product in prods:
        product = get_object_or_404(Product, id=-1)

    context = {
        "page_title": product.name,
        "head_title": product.name,
        "product": product,
        "rel_prods": related_prods,
    }
    return render(request, "products/product_details.html", context)

def search_view(request, param=None):
    request_params = request.GET
    query = request_params.get("q")

    categories = Category.objects.none()
    products = Product.objects.none()

    if not query:
        query = param
    if query:
        categories = Category.objects.filter(name__icontains=query)
        products = Product.objects.filter(Q(name__icontains=query) |
                                          Q(description__icontains=query))

    title = "Search"
    head = "Search *" + str(len(products)) + "*"

    context = {
        "page_title": title,
        "head_title": head,
        "q": query,
        "cats": categories,
        "prods": products,
    }
    return render(request, 'products/search.html', context)


##
## The following methods are support methods, supplying informations for the views above
##
def get_category_obj(categories, cat_name):
    cat_us = cat_name.replace('_', ' ')

    category = categories[-1].replace('_', ' ') if categories else cat_name.replace('_', ' ')
    if categories and len(categories) > 1:
        #parent = categories[-2].replace('_', ' ')
        top_cat = categories[0].replace('_', ' ')

        cat_objs = Category.objects.filter(name=category)

        for cat in cat_objs:
            parent = cat.parent
            while parent:
                if parent.parent:
                    parent = parent.parent
                else:
                    tc = parent
                    parent = None
            if top_cat == tc.name.replace('_', ' '):
                return cat
        return None
    else:
        cat_objs = Category.objects.filter(name=category)
    cat_obj = cat_objs[0]
    return cat_obj

def get_products(prim_cat):
    products = Product.objects.filter(prim_category=prim_cat.id)

    sub_cats = Category.objects.filter(parent=prim_cat.id)
    if sub_cats:
        for cat in sub_cats:
            products = list(chain(products, get_products(cat)))

    return products