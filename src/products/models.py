from django.db import models
from django.core.urlresolvers import reverse

from categories.models import Category

class Manufacturer(models.Model):
    # primary key = pk = id
    name            = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name[:25]

    class Meta:
        verbose_name        = 'Manufacturer'
        verbose_name_plural = 'Manufactores'
        ordering            = ['name']

class Product(models.Model):
    # primary key = pk = id
    name            = models.CharField(max_length=128, unique=True)
    description     = models.TextField()
    image           = models.CharField(max_length=2048, null=True, blank=True)
    price           = models.DecimalField(max_digits=15, decimal_places=2)
    stock           = models.DecimalField(max_digits=15, decimal_places=2)
    sold            = models.IntegerField(default=0)
    manufacturer    = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    prim_category   = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='primary')

    def __str__(self):
        return self.name[:25]

    def __short_descr__(self):
        return self.description[:100]+"..."

    short_descr     = property(__short_descr__)

    def get_absolute_url(self):
        return reverse("category-view", kwargs={"cat": self.prim_category})

    class Meta:
        verbose_name        = 'Product'
        verbose_name_plural = 'Products'
        ordering            = ['sold']
