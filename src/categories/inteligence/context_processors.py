#Get Categories for menu
from categories.models import Category
from django.shortcuts import get_object_or_404

def get_top_cats(request):
    cat_top = Category.objects.filter(parent__isnull=True).order_by('name')
    return { "get_top_cats": cat_top}

def get_side_cats(request):
    #list = add_cat()
    categories = ""
    if 'top_cat' in request.session:
        top_cat_id = request.session['top_cat']
        try:
            top_cat = Category.objects.get(id=top_cat_id)
            href = "/category/" + top_cat.safe_name
            categories = add_cat(top_cat, href)
        except:
            top_cat = None
            categories = "---"
    return { "get_side_cats": categories }

def add_cat(cat=None, ref=None):
    sn = cat.safe_name
    list = "<ul id='" + sn + "' class='list-group'>"
    sub_cats = Category.objects.filter(parent__id=cat.id)

    for cat in sub_cats:
        href = ref + "/" + cat.safe_name
        list += "<li class='list-group-item'><a href='" + href + "'>" + cat.name + "</a>"

        if sub_cats:
            list += add_cat(cat, href)
        list += "</li>"
    list += "</ul>"

    return list