from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.db.models import Q
from urllib import parse

from .models import Category, Tag
from products.models import Product

# Create your views here.


def get_category_obj(cat_name):
    try:
        unsafed = cat_name.replace('_', ' ')
    except:
        return None

    category = Category.objects. get(name=unsafed)
    return category