from django.db import models

from urllib import parse

# Create your models here.
class Category(models.Model):
    name            = models.CharField(max_length=128, unique=False)
    description     = models.CharField(max_length=256, unique=False, null=True, blank=True)
    parent          = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)

    #    def get_absolute_url(self):
    #       print(self.name)
    #      return reverse("category-view", kwargs={"cat": self.name})

    def __str__(self):
        return self.name[:25] + " (" + (self.parent_name if self.parent else "--") + "): " + str(self.id)

    def __safe_name__(self):
        return self.name.replace(' ', '_')

    safe_name = property(__safe_name__)

    def __top_cat__(self):
        top = self
        while top.parent:
            top = top.parent
        return top
    top_cat = property(__top_cat__)

    def __parent_name__(self):
        return self.parent.name
    parent_name = property(__parent_name__)

    class Meta:
        verbose_name        = 'Category'
        verbose_name_plural = 'Categories'
        ordering            = ['name']